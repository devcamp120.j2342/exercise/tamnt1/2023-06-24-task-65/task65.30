package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.user.models.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
