package com.devcamp.user.services;

import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

import com.devcamp.user.models.Order;
import com.devcamp.user.repository.OrderRepository;

@Service
public class OrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Order> getAllOrders() {

        return orderRepository.findAll();
    }

    public Order getOrderById(Long id) {
        Optional<Order> optionalOrder = orderRepository.findById(id);
        return optionalOrder.orElse(null);
    }

    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }

    public Order updateOrder(Long id, Order order) {
        Optional<Order> optionalOrder = orderRepository.findById(id);
        if (optionalOrder.isPresent()) {
            Order existingOrder = optionalOrder.get();
            existingOrder.setOrderCode(order.getOrderCode());
            existingOrder.setPizzaSize(order.getPizzaSize());
            existingOrder.setPizzaType(order.getPizzaType());
            existingOrder.setVoucherCode(order.getVoucherCode());
            existingOrder.setPrice(order.getPrice());
            existingOrder.setPaid(order.getPaid());
            existingOrder.setUser(order.getUser());
            return orderRepository.save(existingOrder);
        } else {
            return null;
        }
    }

    public void deleteOrder(Long id) {
        orderRepository.deleteById(id);
    }
}
